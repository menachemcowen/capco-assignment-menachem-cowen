# Hello, and welcome! I hope you enjoy :)

After running ng build if you are not placing the files at the root of your web server please remember to either change <base href="/"> to the correct path or run
ng build --base-href /capco-assignment-menachem-cowen/

Thank You!
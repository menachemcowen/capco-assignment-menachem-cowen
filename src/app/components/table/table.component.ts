import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { PaginatorService } from '../../services/paginator.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  constructor(private dataService: DataService, private pagerService: PaginatorService) { }

  private allItems: any;
  pager: any = {};
  headerItems = ['Name', 'Email', 'Company'];
  pagedItems: any[];
  numberOfRows = 5;
  numOfRowOptions = [5, 10, 15, 20, 25, 30];

  ngOnInit() {
    this.dataService.getFakeData().subscribe((data) => {
      this.allItems = data;
      this.setPage(1);

      this.dataService.getFakeData()
        .subscribe((fakeData) => this.allItems = fakeData);
    });
  }
  setPage(page: number) {
    this.pager = this.pagerService.getPaginatorProperties(this.allItems.length, page, this.numberOfRows);
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
  onOptionChange() {
    this.setPage(this.pager.currentPage);
  }
  sendRowData(id, status) {
    this.dataService.sendData(id, status);
  }
}


import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(public http: HttpClient) { }
  getFakeData() {
      return this.http.get('assets/sample_data.json');
  }
  sendData(id, status) {
    this.http.post('http://localhost:3000/api/submit',
    {
        'rowId': id,
        'rowStatus': status,
    })
    .subscribe(
      data => {
          console.log('/api/submit - Success', data);
      },
      error => {
          console.log('/api/submit - Error', error);
      }
  );
  }
}
